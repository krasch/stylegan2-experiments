import sys
sys.path.append("../../stylegan2")

from pathlib import Path

import dnnlib.tflib as tflib
import numpy as np
import PIL.Image

import pretrained_networks

Z_SIZE = 512
noise_rng = np.random.RandomState(1234)
results_dir = Path("../results/")

# results_dir = Path("/home/arbeit/prototypefund/results/stylegan2/results")


def init_generator():
    _G, _D, Gs = pretrained_networks.load_networks("gdrive:networks/stylegan2-ffhq-config-f.pkl")
    noise_vars = [var for name, var in Gs.components.synthesis.vars.items() if name.startswith('noise')]
    output_transform = dict(func=tflib.convert_images_to_uint8, nchw_to_nhwc=True)

    def generate(z):
        tflib.set_vars({var: noise_rng.randn(*var.shape.as_list()) for var in noise_vars})
        images = Gs.run(z, None, randomize_noise=False, output_transform=output_transform,  truncation_psi=0.5)
        return images

    return generate


def store_latent(z, image_id, results_dir):
    np.save(str(results_dir / str(image_id)), z)


def store_image(image, image_id, results_dir):
    file = str((results_dir / str(image_id))) + ".png"
    PIL.Image.fromarray(image, 'RGB').save(str(file))


def generate_people(num_people, people_dir: Path):
    people_dir.mkdir(exist_ok=True, parents=True)

    generator = init_generator()
    z_rng = np.random.RandomState(2345)

    batch_size = 10
    num_batches = int(num_people / batch_size) + 1

    for current_batch_id in range(num_batches):
        print("Generating batch {}/{}".format(current_batch_id, num_batches))

        z = z_rng.randn(batch_size, Z_SIZE)
        images = generator(z)

        for i in range(len(images)):
            store_image(images[i], current_batch_id * batch_size + i, results_dir)
            store_latent(z[i], current_batch_id * batch_size + i, results_dir)


def load_people(people_dir: Path):
    people = people_dir.glob("*.npy")
    return [(f.with_suffix("").name, np.load(f)) for f in sorted(people)]


def interpolate(p1, p2, interpolation_dir: Path):
    interpolation_dir.mkdir(exist_ok=True, parents=True)

    num_steps = 10

    id1, z1 = p1
    id2, z2 = p2
    print("Interpolating {} <=> {}".format(id1, id2))

    zs = np.zeros((num_steps, Z_SIZE))
    alphas = np.linspace(start=0.0, stop=1.0, num=num_steps)
    for i, alpha in enumerate(alphas):
        zs[i, :] = alpha * z2 + (1.0 - alpha) * z1

    generator = init_generator()
    images = generator(zs)

    for i in range(len(images)):
        name = "{}_{}_{}".format(id1, id2, alphas[i])
        store_image(images[i], name, interpolation_dir)
        store_latent(zs[i], name, interpolation_dir)


# generate_people(1000, results_dir / "people")

known_people = load_people(results_dir / "people")

for j in range(10):
    interpolate(known_people[j], known_people[20+j], results_dir / "interpolation")
